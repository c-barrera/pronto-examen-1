import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'fs1';
  name: string;
  searchText: string = "";
  newText: string = '';
  selected_items = [];
  selected_actives = [];
  isSearch: boolean = false;
  id: number = 1;

  items = [
    {
      name: 'Learn Javascript',
      id: this.id++,
      selected: false
    },
    {
      name: 'Learn React',
      id: this.id++,
      selected: false
    },
    {
      name: 'Build a React App',
      id: this.id++,
      selected: false
    }
  ];

  ngOnInit() {
    this.selected_actives = this.items.filter(s => {
      return !s.selected;
    });
    localStorage.setItem('__items', JSON.stringify(this.items));
  }

  changeState(state: boolean) {
    this.isSearch = state;
    this.newText = '';
    this.searchText = '';
  }

  onEnter() {
    if (this.newText.length > 0) {
      this.items.push({
        name: this.newText,
        id: this.id++,
        selected: false
      });
      this.newText = '';
      this.getSelected();
    }
  }

  getSelected() {
    this.selected_items = this.items.filter(s => {
      return s.selected;
    });
    this.selected_actives = this.items.filter(s => {
      return !s.selected;
    });
    localStorage.setItem('__items', JSON.stringify(this.items));
  }

  getCompleted() {
    this.items = this.selected_items;
  }

  getActives() {
    this.items = this.selected_actives;
  }

  getAll() {
    this.items = JSON.parse(localStorage.getItem('__items'));
    this.getSelected();
  }
}